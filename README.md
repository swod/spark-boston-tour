# spark-boston-tour

Calculate some metrics for Boston crimes data.

# Run
sbt package / sbt assembly

spark-submit --master local[*] target/scala-2.11/spark-boston-tour_2.11-0.0.1.jar <path/to/crimes.scv> <path/to/offense_codes.csv> <path/to/outputdir>
