package com.rsw

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._

import org.apache.spark.sql.Column
import org.apache.spark.sql.Row
import org.apache.spark.sql.catalyst.expressions.aggregate.ApproximatePercentile


object PercentileApprox {
  def percentile_approx(col: Column, percentage: Column, accuracy: Column): Column = {
    val expr = new ApproximatePercentile(
      col.expr,  percentage.expr, accuracy.expr
    ).toAggregateExpression
    new Column(expr)
  }
  def percentile_approx(col: Column, percentage: Column): Column = percentile_approx(
    col, percentage, lit(ApproximatePercentile.DEFAULT_PERCENTILE_ACCURACY)
  )
}


// INCIDENT_NUMBER,OFFENSE_CODE,OFFENSE_CODE_GROUP,OFFENSE_DESCRIPTION,
// DISTRICT,REPORTING_AREA,SHOOTING,OCCURRED_ON_DATE,
// YEAR,MONTH,DAY_OF_WEEK,HOUR,UCR_PART,STREET,Lat,Long,Location
case class CrimeRecord(
  DISTRICT: String, CRIME_TYPE: String, count: Long
)


object BostonTour extends App with SparkSessionWrapper {
  import spark.implicits._

  override def main (args: Array[String]) {
    spark.sparkContext.setLogLevel("WARN")

    var crimesPath = "./data/crime.csv"
    var offenseCodesPath = "./data/offense_codes.csv"
    var outputPath = "./output/result"
    if (args.length == 3) {
      crimesPath = args(0)
      offenseCodesPath = args(1)
      outputPath = args(2)
    }

    val totalCrimes = makeCrimesTotal(crimesPath)
    val monthlyDf = makeCrimesMonthly(crimesPath)
    val freqDf = makeFrequentCrimeTypes(crimesPath, offenseCodesPath)
    val latDf = makeCrimesLat(crimesPath)
    val longDf = makeCrimesLong(crimesPath)

    totalCrimes
      .join(monthlyDf, totalCrimes("DISTRICT") <=> monthlyDf("DISTRICT")).drop(monthlyDf("DISTRICT"))
      .join(freqDf, totalCrimes("DISTRICT") <=> freqDf("DISTRICT")).drop(freqDf("DISTRICT"))
      .join(latDf, totalCrimes("DISTRICT") <=> latDf("DISTRICT")).drop(latDf("DISTRICT"))
      .join(longDf, totalCrimes("DISTRICT") <=> longDf("DISTRICT")).drop(longDf("DISTRICT"))
      .orderBy(totalCrimes("DISTRICT"))
      .coalesce(1).write.mode("overwrite").parquet(outputPath)

    spark.read
      .option("header", "true")
      .option("inferSchema", "true")
      .parquet(outputPath).show(false)

    // System.in.read()
    // spark.stop()
  }

  def makeCrimesTotal(crimesPath: String): DataFrame = {
    val crimes = spark.read
      .option("header", "true")
      .option("inferSchema", "true")
      .csv(crimesPath).cache()

    crimes.groupBy("DISTRICT").agg(count("*").alias("total_count"))
  }

  def makeCrimesMonthly(crimesPath: String): DataFrame = {
    import PercentileApprox._

    val crimes = spark.read
      .option("header", "true")
      .option("inferSchema", "true")
      .csv(crimesPath).cache()

    crimes
      .groupBy("YEAR", "MONTH", "DISTRICT")
      .count
      .groupBy("DISTRICT")
      .agg(percentile_approx($"count", lit(0.5)).alias("median_per_month"))
      .orderBy("DISTRICT")

    // val groupedCrimes = crimes
    //   .groupBy("YEAR", "MONTH", "DISTRICT")
    //   .count
    //   .createOrReplaceTempView("groupedCrimes")
    // spark.sql(
    //   "select DISTRICT, percentile_approx(count, 0.5) from groupedCrimes group by DISTRICT order by DISTRICT desc"
    // ).show
  }

  def makeFrequentCrimeTypes(crimesPath: String, offenseCodesPath: String): DataFrame =  {
    val crimes = spark.read
      .option("header", "true")
      .option("inferSchema", "true")
      .csv(crimesPath).cache()
    val offenseCodes = spark.read
      .option("header", "true")
      .option("inferSchema", "true")
      .csv(offenseCodesPath).cache()
    val broadcastOc = broadcast(offenseCodes)

    crimes
      .join(broadcastOc, crimes("OFFENSE_CODE") === broadcastOc("CODE"))
      // .join(offenseCodes, crimes("OFFENSE_CODE") === offenseCodes("CODE"))
      .select("INCIDENT_NUMBER", "OFFENSE_CODE", "DISTRICT", "NAME")
      .withColumn("CRIME_TYPE", trim(split($"NAME", "-")(0)))
      .groupBy("DISTRICT", "CRIME_TYPE")
      .count
      .as[CrimeRecord]
      .groupByKey(r => r.DISTRICT)
      .flatMapGroups {
        case (district, elements) =>
          List(
            elements.toList.sortBy(x => -x.count).take(3)
              .reduce(
                (accum, record) => {
                  CrimeRecord(
                    accum.DISTRICT,
                    accum.CRIME_TYPE + ", " + record.CRIME_TYPE,
                    accum.count + record.count
                  )
                }
              )
          )
      }
      .drop("count")
      .toDF()
  }

  def makeCrimesLat(crimesPath: String): DataFrame = {
    val crimes = spark.read
      .option("header", "true")
      .option("inferSchema", "true")
      .csv(crimesPath).cache()

    crimes
      .groupBy("DISTRICT")
      .agg(avg("Lat").alias("average_lat"))
      .select("DISTRICT", "average_lat")
  }

  def makeCrimesLong(crimesPath: String): DataFrame =  {
    val crimes = spark.read
      .option("header", "true")
      .option("inferSchema", "true")
      .csv(crimesPath).cache()

    crimes
      .groupBy("DISTRICT")
      .agg(avg("Long").alias("average_long"))
      .select("DISTRICT", "average_long")
  }
}
